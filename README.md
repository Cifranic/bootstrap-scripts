# For Centos7 in AWS 

```
# first script to run
sudo yum -y install wget && sh -c "$(wget https://gitlab.com/Cifranic/bootstrap-scripts/raw/master/aws-centos7/00-bootstrap-centos7.sh -O -)"


# run manually after first script, and zsh shell is present 
sh -c "$(wget https://gitlab.com/Cifranic/bootstrap-scripts/raw/master/aws-centos7/01-configure-zsh.sh -O -)" && source ~/.zshrc

# enable autocomplete for aws command:  
source /usr/local/bin/aws_zsh_completer.sh



```


