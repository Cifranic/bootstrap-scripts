#!/bin/bash

# get dependencies 
sudo yum -y install epel-release 
sudo yum -y install git tmux wget zsh mlocate

# update for locate command
sudo updatedb

# allow numbers in vim 
echo "set number" > ~/.vimrc

# pull oh-my-zsh, and install
sh -c "$(wget https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"
