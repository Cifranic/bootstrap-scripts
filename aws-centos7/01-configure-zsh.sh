#!/bin/bash

# give default theme 
sed -i 's/robbyrussell/jispwoso/g' ~/.zshrc

# set alias'
echo "alias vi=\"vim\"" >> ~/.zshrc
